Extended Commands Mod v1.1
By Leslie E. Krause

Extended commands adds a suite of multiplayer-specific chat commmands for use on servers.

Repository
----------------------

Browse source code...
  https://bitbucket.org/sorcerykid/xcommands

Download archive...
  https://bitbucket.org/sorcerykid/xcommands/get/master.zip
  https://bitbucket.org/sorcerykid/xcommands/get/master.tar.gz

Compatability
----------------------

Minetest 0.4.14+ required

Installation
----------------------

  1) Unzip the archive into the mods directory of your game
  2) Rename the xcommands-master directory to "xcommands"

License of source code
----------------------------------------------------------

GNU Lesser General Public License v3 (LGPL-3.0)

Copyright (c) 2018-2019, Leslie E. Krause

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU Lesser General Public License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

http://www.gnu.org/licenses/lgpl-2.1.html
